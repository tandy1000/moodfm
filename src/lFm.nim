import asyncdispatch, httpclient, os, json
import lastfm
import lastfm/[track, user]
import utils

proc authorise*(
  file: string): LastFMSession =
  let
    keys = parseJson(readFile(file))
    apiKey = keys["apiKey"].getStr()
    apiSecret = keys["apiSecret"].getStr()
    sessionKey = keys["sessionKey"].getStr()
  result = LastFM(apiKey, apiSecret).loadAuth(sessionKey)

proc getAlbumCover(
  trackID, trackCoverURL: string): bool =
  result = false
  if trackCoverURL != "":
    if fileExists(trackID):
      result = true
    else:
      let c = newHttpClient()
      c.downloadFile(trackCoverURL, "/tmp/" & trackID & ".png")
      result = true

proc getTrackDuration(
  auth: LastFMSession,
  trackName, trackArtist: string): int =
  let trackInfo = (waitFor trackInfo(auth, trackName, trackArtist))["track"]
  result = trackInfo["duration"].getInt()

proc getLastTrack*(
  auth: LastFMSession,
  username: string): (string, string, int, bool) =
  let
    lastTrack = (waitFor userRecentTracks(auth, username, limit=1)){"recenttracks", "track"}[0]
    trackName = lastTrack["name"].getStr()
    trackArtist = lastTrack{"artist", "#text"}.getStr()
    trackDuration = getTrackDuration(auth, trackName, trackArtist)
    trackID = trackName & " - " & trackArtist
    trackAlbum = lastTrack{"album", "#text"}.getStr()
    trackCoverURL = lastTrack["image"][2]["#text"].getStr()
    albumCover = getAlbumCover(trackID, trackCoverURL)
  return (trackID, trackAlbum, trackDuration, albumCover)

# let
#   file: string = "keys.json"
#   username: string = "tandy1000"
#   auth: LastFMSession = authorise(file)
#   (mbid, trackName, trackArtist, trackAlbum, trackCoverURL) = getLastTrack(auth, username)
#   trackDuration = getTrackDuration(auth, trackName, trackArtist)