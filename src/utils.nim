import gintro/[gtk, gdk]
from strutils import intToStr, parseInt

const CSSFILE = "assets/style.css"

proc setCSS*(w: gtk.Window) =
  var provider = newCssProvider()
  discard provider.loadFromPath(CSSFILE)
  addProviderForScreen(getScreen(w), provider,
    STYLE_PROVIDER_PRIORITY_APPLICATION)

proc msToMins*(milliseconds: int): string =
  var seconds: string = intToStr((milliseconds div 1000) mod 60)
  if parseInt(seconds) < 10:
    seconds = '0' & seconds
  let minutes = intToStr((milliseconds div (1000 * 60)) mod 60)
  result = minutes & ":" & seconds