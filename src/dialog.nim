import gintro/[gtk, gobject, gio]
import utils

var user: string

proc entryCheck(app: Application) =
  if user != "":
    quit(app)

proc enterPress(e: Entry, app: Application) =
  user = e.text
  entryCheck(app)

proc goClick(b: Button, objects: tuple[e: Entry, app: Application]) =
  user = objects.e.text
  entryCheck(objects.app)

proc dialog*(app: Application): string =
  let
    builder = newBuilderFromFile("assets/dialog.ui")
    window = builder.getApplicationWindow("dialog")
  var
    entry: Entry = builder.getEntry("userEntry")
    go: Button = builder.getButton("go")
  entry.connect("activate", enterPress, app)
  go.connect("clicked", goClick, (entry, app))
  builder.setApplication(app)
  setCSS(window)
  window.showAll
  return user