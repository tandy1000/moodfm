import db_sqlite

proc initialiseDB*(file: string): DbConn =
  let db = open(file, "", "", "")
  db.exec(sql"""CREATE TABLE IF NOT EXISTS Tracks (
                  trackID TEXT PRIMARY KEY,
                  album TEXT,
                  duration INT)""")
  db.exec(sql"""CREATE TABLE IF NOT EXISTS Moods (
                  moodID TEXT PRIMARY KEY,
                  mood TEXT,
                  trackId TEXT,
                  FOREIGN KEY (trackId) REFERENCES Tracks(trackID))""")
  return db

proc insertTrack*(db: DbConn, trackID: string, trackAlbum: string, trackDuration: int) =
  db.exec(sql"INSERT OR REPLACE INTO Tracks VALUES (?,?,?)", trackID, trackAlbum, trackDuration)

proc insertMood*(db: DbConn, moodID: string, mood: string, trackID: string) =
  db.exec(sql"INSERT OR REPLACE INTO Moods VALUES (?,?,?)", moodID, mood, trackID)

proc closeDB*(db: DbConn) =
  db.close()