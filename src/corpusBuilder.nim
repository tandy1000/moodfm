import gintro/[gtk, glib, gobject, gdkpixbuf]
from strutils import repeat
from lastfm import LastFMSession
from db_sqlite import DbConn
import utils, lFm, dbMan

var
  Auth: LastFMSession
  user: string

let
  moods = @["Happy", "Exciting", "Sad", "Bittersweet", "Intense", "Angry",
            "Groovy", "Dancey", "Confident", "Calm", "Weird", "Anxious"]

proc buttonClick(b: Button, mood: string) =
  echo mood
  #insertMood(db, mood, trackID)

proc updater(interval: var int, mainWindow: Window, albumName: Label, songName: Label, playBar: Label, duration: Label, cover: Image) =
  let
    BarLength = 28
    (trackID, trackAlbum, trackDuration, albumCover) = getLastTrack(Auth, user)
    db = initialiseDB(user & ".db")
    length = len(trackID)
  insertTrack(db, trackID, trackAlbum, trackDuration)

  songName.setText(trackID)
  duration.setText(msToMins(trackDuration))
  if trackAlbum != "":
    albumName.setText(trackAlbum)
  else:
    albumName.setText("Album not found.")
  if albumCover:
    cover.setFromFile("/tmp/" & trackID & ".png")
  else:
    cover.setFromFile("src/assets/no_cover.png")
  if length > BarLength:
    playBar.setText("--".repeat(length))
  else:
    playBar.setText("--".repeat(BarLength))
  if trackDuration != 0:
    interval = ((trackDuration div 1000) div 6)
  showAll(mainWindow)

proc corpusBuilder*(app: Application, u: string, keyPath: string) =
  Auth = authorise(keyPath)
  user = u
  let builder = newBuilder()
  discard builder.addFromFile("src/assets/app.ui")
  let mainWindow = builder.getApplicationWindow("window")
  mainWindow.setApplication(app)
  setCSS(mainWindow)
  let gif = newPixbufAnimationFromFile("src/assets/scrobbling.gif")
  var
    albumName: Label = builder.getLabel("albumName")
    songName: Label = builder.getLabel("songName")
    playBar: Label = builder.getLabel("playBar")
    duration: Label = builder.getlabel("duration")
    cover: Image = builder.getImage("cover")
    scrobbling: Image = builder.getImage("scrobbling")
    button: Button
    interval: int = 30
  scrobbling.setFromAnimation(gif)
  for i, mood in moods:
    button = builder.getButton("button" & $i)
    button.setLabel(mood)
    button.connect("clicked", buttonClick, mood)
  updater(interval, mainWindow, albumName, songName, playBar, duration, cover)